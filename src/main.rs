pub mod tensor;
pub mod idx_reader;

extern crate rusty_machine;

use std::path::Path;
use std::fs::File;
use rusty_machine::linalg::Matrix;
use rusty_machine::learning::nnet::{NeuralNet, BCECriterion};
use rusty_machine::learning::toolkit::regularization::Regularization;
use rusty_machine::learning::toolkit::activ_fn::Sigmoid;
use rusty_machine::learning::optim::grad_desc::StochasticGD;
use rusty_machine::learning::SupModel;

fn main() {
    let mut input_file = File::open(Path::new("data/train-images-idx3-ubyte")).unwrap();
    let input_tensor_u8 = idx_reader::load_tensor(&mut input_file).to_u8().unwrap();
    let input_tensor = input_tensor_u8.transform(|&v| v as f64 / 255.0);
    let input_count = input_tensor.size(0);
    let input_layer_size = input_tensor.dim_width(0);
    let inputs = Matrix::new(input_count, input_layer_size, input_tensor.get_data());

    let mut target_file = File::open(Path::new("data/train-labels-idx1-ubyte")).unwrap();
    let target_tensor_u8 = idx_reader::load_tensor(&mut target_file).to_u8().unwrap();
    let target_tensor = target_tensor_u8.extend(10, |&v| {
        let mut r = [0.0; 10];
        r[v as usize] = 1.0;
        r.to_vec()
    });
    let target_count = target_tensor.size(0);
    let target_layer_size = target_tensor.dim_width(0);
    let targets = Matrix::new(target_count, target_layer_size, target_tensor.get_data());

    let layers = &[784,  10];
    let criterion = BCECriterion::new(Regularization::L2(0.));
    let mut model = NeuralNet::default(layers);

    println!("Starting training...");
    model.train(&inputs, &targets);
}
