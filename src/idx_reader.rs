use tensor::*;
use std::io::Read;
use std::mem;

#[derive(Debug, Copy, Clone)]
enum DataType {
    UnsignedByte = 0x08,
    SignedByte = 0x09,
    Short = 0x0B,
    Int = 0x0C,
    Float = 0x0D,
    Double = 0x0E
}

impl DataType {
    fn from(v: u8) -> Option<DataType> {
        match v {
            0x08 => Some(DataType::UnsignedByte),
            0x09 => Some(DataType::SignedByte),
            0x0B => Some(DataType::Short),
            0x0C => Some(DataType::Int),
            0x0D => Some(DataType::Float),
            0x0E => Some(DataType::Double),
            _    => None
        }
    }

    fn size(&self) -> usize {
        match *self {
            DataType::UnsignedByte => 1,
            DataType::SignedByte => 1,
            DataType::Short => 2,
            DataType::Int => 4,
            DataType::Float => 4,
            DataType::Double => 8,
        }
    }
}

pub fn load_tensor(reader: &mut Read) -> DynamicTensor {
    let (data_type, dim_count) = read_header(reader);
    println!("DataType: {:?}, Dimensions: {}", data_type, dim_count);
    let dim_sizes = read_dim_sizes(reader, dim_count);
    println!("Dim Sizes: {:?}", dim_sizes);
    let val_count = dim_sizes.iter().fold(1, |acc, d| acc * d);
    println!("value count: {}", val_count);

    match data_type {
        DataType::UnsignedByte => DynamicTensor::TensorUByte(TensorU8::new_with_data(
            dim_sizes,
            read_data(reader, data_type, val_count)
        )),
        DataType::SignedByte => DynamicTensor::TensorByte(TensorI8::new_with_data(
            dim_sizes,
            read_data(reader, data_type, val_count)
        )),
        DataType::Short => DynamicTensor::TensorShort(TensorI16::new_with_data(
            dim_sizes,
            read_data(reader, data_type, val_count)
        )),
        DataType::Int => DynamicTensor::TensorInt(TensorI32::new_with_data(
            dim_sizes,
            read_data(reader, data_type, val_count)
        )),
        DataType::Float => DynamicTensor::TensorFloat(TensorF32::new_with_data(
            dim_sizes,
            read_data(reader, data_type, val_count)
        )),
        DataType::Double => DynamicTensor::TensorDouble(TensorF64::new_with_data(
            dim_sizes,
            read_data(reader, data_type, val_count)
        ))
    }
}

fn read_header(reader: &mut Read) -> (DataType, u8) {
    let mut buf = [0u8; 4];
    let mut chunk = reader.take(4);
    let n = chunk.read(&mut buf).expect("Didn't read enough");
    assert_eq!(4, n);

    let data_type = DataType::from(buf[2]).unwrap();

    (data_type, buf[3])
}

fn read_dim_sizes(reader: &mut Read, dim_count: u8) -> Vec<usize> {
    let mut r = Vec::with_capacity(dim_count as usize);
    for _d in 0..dim_count {
        r.push(read_integer(reader) as usize);
    }

    r
}

fn read_data<T>(reader: &mut Read, data_type: DataType, val_count: usize) -> Vec<T> {
    // Calculate buffer size
    let buff_len = data_type.size() * val_count;
    let mut buff: Vec<u8> = Vec::with_capacity(buff_len);

    let n = reader.read_to_end(&mut buff).unwrap();
    assert_eq!(buff_len, n);

    unsafe {
        let ptr = buff.as_mut_ptr();
        mem::forget(buff);
        Vec::from_raw_parts(ptr as *mut T, val_count, val_count)
    }
}

fn read_integer(reader: &mut Read) -> u32 {
    let mut buf = [0u8; 4];
    let mut chunk = reader.take(4);
    let n = chunk.read(&mut buf).expect("Didn't read enough");
    assert_eq!(4, n);

    ((buf[0] as u32) << 24) + ((buf[1] as u32) << 16) + ((buf[2] as u32) << 8) + (buf[3] as u32)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::Path;
    use std::fs::File;

    #[test]
    fn blah() {
        let mut f = File::open(Path::new("data/train-images-idx3-ubyte")).unwrap();
        let t = load_tensor(&mut f);
    }
}
