pub struct Tensor<T: Copy> {
    dims: Vec<usize>,
    data: Vec<T>
}

pub type TensorU8 = Tensor<u8>;
pub type TensorI8 = Tensor<i8>;
pub type TensorI16 = Tensor<i16>;
pub type TensorI32 = Tensor<i32>;
pub type TensorF32 = Tensor<f32>;
pub type TensorF64 = Tensor<f64>;

pub enum DynamicTensor {
    TensorUByte(TensorU8),
    TensorByte(TensorI8),
    TensorShort(TensorI16),
    TensorInt(TensorI32),
    TensorFloat(TensorF32),
    TensorDouble(TensorF64)
}

impl DynamicTensor {
    pub fn to_u8(self) -> Option<TensorU8> {
        match self {
            DynamicTensor::TensorUByte(tensor) => Some(tensor),
            _ => None
        }
    }
}

impl <T: Copy> Tensor<T> {
    pub fn new(dims: Vec<usize>) -> Tensor<T> {
        Tensor {
            dims: dims,
            data: Vec::new()
        }
    }

    pub fn new_with_data(dims: Vec<usize>, data: Vec<T>) -> Tensor<T> {
        Tensor {
            dims: dims,
            data: data
        }
    }

    pub fn sub(&self, idx: &[usize]) -> Tensor<T> {
        assert!(
            idx.len() < self.dims.len(),
            "Tensor::sub idx size cannot be greater than or equal to the tensors dimensions"
        );

        let mut offset = 0;
        for (dim, &i) in idx.iter().enumerate() {
            offset += i * self.dims[dim+1..].iter().fold(1, |acc, &x| acc * x);
        }
        let len = self.dims[idx.len()..].iter().fold(1, |acc, &dim_size| acc * dim_size);

        let sub_dims = &self.dims[idx.len()..];

        Tensor::new_with_data(sub_dims.to_vec(), self.data[offset..offset+len].to_vec())
    }

    /// Divulge the internal tensor data. This drops the tensor. 
    pub fn get_data(self) -> Vec<T> {
        return self.data;
    }

    pub fn size(&self, dim: usize) -> usize {
        assert!(dim < self.dims.len(), "Tensor only has {} dimensions.", self.dims.len());
        self.dims[dim]
    }

    pub fn dim_width(&self, dim: usize) -> usize {
        assert!(dim < self.dims.len(), "Tensor only has {} dimensions.", self.dims.len());
        if dim == self.dims.len() - 1 {
            1
        } else {
            self.dims[dim+1..].iter().fold(1, |acc, &dim_size| acc * dim_size)
        }
    }

    pub fn get(&self, i: usize) -> &[T] {
        let sub_size = self.dims[1..].iter().fold(1, |acc, &x| acc * x);
        let offset = i * sub_size;

        &self.data[offset..offset+sub_size]
    }

    pub fn get_dims(&self) -> &Vec<usize> {
        &self.dims
    }

    pub fn transform<S: Copy, F>(&self, transformer: F) -> Tensor<S>
        where F : Fn(&T) -> S {
        let mut d = Vec::with_capacity(self.data.len());
        for v in self.data.iter() {
            d.push(transformer(v));
        }

        Tensor::new_with_data(self.dims.to_owned(), d)
    }

    pub fn extend<S: Copy, F>(self, new_dim_size: usize, extender: F) -> Tensor<S>
        where F : Fn(&T) -> Vec<S> {
        let mut data = Vec::with_capacity(self.data.len() * new_dim_size);
        for v in self.data.iter() {
            data.extend(extender(v));
        }

        let mut dims = self.dims.to_owned();
        dims.push(new_dim_size);
        Tensor::new_with_data(dims, data)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_with_data_should_work() {
        let t = Tensor::new_with_data(vec![5,2], vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

        assert!(t.dims[0] == 5);
        assert!(t.dims[1] == 2);
    }

    #[test]
    fn sub_0dims_should_return_same_tensor() {
        // Arrange
        let t = Tensor::new_with_data(vec![5,2], vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

        // Act
        let s = t.sub(&[]);

        // Assert
        assert!(s.dims.len() == 2);
        assert!(s.data.len() == 10);
    }

    #[test]
    fn sub_1_dim_should_work() {
        // Arrange
        let t = Tensor::new_with_data(vec![5,2], vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

        // Act
        let s = t.sub(&[2]);

        // Assert
        assert!(s.dims[0] == 2);
        assert!(s.data[0] == 5);
        assert!(s.data[1] == 6);
    }

    #[test]
    fn sub_2_dims_should_work() {
        // Arrange
        let t = Tensor::new_with_data(vec![3,2,2], vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);

        // Act
        let s = t.sub(&[1, 1]);

        // Assert
        assert!(s.dims[0] == 2);
        assert!(s.data[0] == 7);
        assert!(s.data[1] == 8);
    }

    #[test]
    fn get_1_sub_dims_should_work() {
        // Arrange
        let t = Tensor::new_with_data(vec![5,2], vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

        // Act
        let v = t.get(3);

        // Assert
        assert!(v.len() == 2);
        assert!(v[0] == 7);
        assert!(v[1] == 8);
    }

    #[test]
    fn get_2_sub_dims_should_work() {
        // Arrange
        let t = Tensor::new_with_data(vec![3,2,2], vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);

        // Act
        let v = t.get(1);

        // Assert
        assert!(v.len() == 4);
        assert!(v[0] == 5);
        assert!(v[1] == 6);
        assert!(v[2] == 7);
        assert!(v[3] == 8);
    }
}
